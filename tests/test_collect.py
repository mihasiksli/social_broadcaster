#! coding: utf-8
from __future__ import unicode_literals

import unittest
import collect_social
import settings
import broadcast_messenger


class TestCollectVK(unittest.TestCase):
    def setUp(self):
        self.source_type = 'vk'
        self.source_link = '-68305318'

    def tesst_collect(self):
        print self.source_link
        data_list = collect_social.collect_data(self.source_type, self.source_link)
        print data_list
        # print data_list


class TestBroadcastTelegram(unittest.TestCase):
    def setUp(self):
        self.source_type = 'vk'
        # self.source_link = '-125004421'
        self.source_link = '-68305318'
        # self.telegram_channel = '@lentachold'
        self.telegram_channel = '@mk_test'
        self.telegram_bot = settings.TELEGRAM_BOTS[0].get('token')
        # self.telegram_bot = '258753817:AAEVABt4e0j3ELd8hKPHt5k0b6kMZeAn6bE'
        self.href_text = 'Лентаче'

    def test_broadcast(self):
        d = collect_social.collect_data(self.source_type, self.source_link)[0]
        print d
        broadcast_messenger.broadcast_telegram(d, self.telegram_channel, self.telegram_bot, self.href_text)

if __name__ == '__main__':
    unittest.main()
