from redis import Redis
from rq import Queue
from work_data import DATA
from tasks.tasks_messenger import task_telegram
from time import sleep


def start_task_telegram():
    q = Queue(connection=Redis())
    for wd in DATA:
        q.enqueue(task_telegram,
                  wd.get('source_type'),
                  wd.get('source_link'),
                  wd.get('channel'),
                  wd.get('bot_token'),
                  wd.get('href_text'))


def start_task_simple():

    for wd in DATA:
        task_telegram(wd.get('source_type'),
                      wd.get('source_link'),
                      wd.get('channel'),
                      wd.get('bot_token'),
                      wd.get('href_text'))
        sleep(10)

# start_task_telegram()

