# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

import broadcast_messenger
import collect_social
import settings
import os


def task_telegram(source_type, source_link, channel, bot_token, href_text):
    print 'task telegrom'

    def get_last_id():
        try:
            with open(data_file_name) as f:
                lines = f.readlines()
                try:
                    return int(lines[-1].strip())
                except (IndexError, ValueError, TypeError):
                    pass
        except IOError:
            pass
        return None

    def save_ids():
        try:
            with open(data_file_name, 'a') as f:
                for d in data_list:
                    f.write('%s\n' % d.get('id'))
        except IOError:
            pass
    data_file_name = os.path.join(settings.BROADCAST_DATA_DIRECTORY, '%s_%s.txt' % (source_type, source_link))
    data_list = collect_social.collect_data(source_type, source_link, last_id=get_last_id())
    print 'lastid', get_last_id()
    if not data_list:
        print 'empty'
        return
    for message_data in data_list:
        try:
            save_ids()
            broadcast_messenger.broadcast_telegram(message_data, channel, bot_token, href_text)
        except Exception as e:
            print 'EXCEPTION: ', str(e)
            pass
