# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests
from collections import defaultdict
import settings


def get_response_json(url, params):
    r = requests.get(url, params=params)
    return r.json()


def collect_data(source_type, source_link, last_id=None):
    def collect_vk():
        if source_type != 'vk':
            return None
        r = requests.get('https://api.vk.com/method/wall.get', params={'owner_id': source_link, 'v': '5.52'})
        if not r.status_code == 200 or not r.json().get('response'):
            print 'ERROR COLLECT'
            return None
        items = []
        if last_id:
            for i in r.json().get('response', {}).get('items'):
                if int(i.get('id')) > int(last_id):
                    items.append(i)
        else:
            items = [max(r.json().get('response', {}).get('items'), key=lambda x: x.get('id'))]
        items = sorted(items, key=lambda x: x.get('id'))
        print 'items', items
        return [adapt_data(i, source_type) for i in items] or None

    def collect_facebook():
        return None

    def collect_twitter():
        return None

    return collect_vk() or collect_facebook() or collect_twitter()


def adapt_data(d, source_type):
    def adapt_data_vk():
        def get_attachments():
            attachments = d.get('attachments', []) or []
            attachment_dict = defaultdict(list)
            for a in attachments:
                if a.get('page'):
                    attachment_dict['page'] = {'title': a.get('page').get('title'),
                                               'url': a.get('page').get('view_url')}
                if a.get('link'):
                    attachment_dict['link'] = a.get('link').get('url')
                if a.get('video'):
                    r = requests.get('https://api.vk.com/method/video.get', params={
                        'v': '5.52',
                        'access_token': settings.VK_ACCESS_TOKEN,
                        'videos': '%s_%s' % (a.get('video').get('owner_id'), a.get('video').get('id')),
                    })
                    if r.json().get('response', {}).get('items'):
                        attachment_dict['video'].append(r.json().get('response', {}).get('items')[0].get('player'))
                if a.get('photo'):
                    for size in ['1280', '807', '604', '2560', '130']:
                        ph = a.get('photo').get('photo_%s' % size)
                        if ph:
                            attachment_dict['photo'].append(ph)
                            break

            return attachment_dict

        if source_type != 'vk':
            return None
        data = {
            'id': d.get('id'),
            'text': d.get('text'),
            'href': 'https://vk.com/public%s?w=wall%s_%s' % (str(d.get('owner_id')).strip('-'), d.get('owner_id'), d.get('id'))
        }
        data.update(get_attachments())
        return data

    return adapt_data_vk()
