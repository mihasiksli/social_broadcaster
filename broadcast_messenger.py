# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import telebot
import requests
import os
import settings
import re
import time

URL_PATTERN = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'


def broadcast_telegram(message_data, channel, bot_token, href_text):
    pattern_vk = r'\[id\d+\|(?P<name>[\w\s]+)\]'

    print 'broodcost'

    # print message_data

    def format_text(only_text=False):
        if only_text:
            t = message_data.get('text')
            t = re.sub(pattern_vk, '\g<name>', t,  flags=re.UNICODE | re.IGNORECASE)
            text_list = [t]
            for splitter in ['. ', '\n', '.\n']:
                text_list = t.split(splitter, 1)
                if len(text_list) > 1:
                    break
            caption = text_list[0]
            rest_text = text_list[1] if len(text_list) > 1 else ''
            t = '<b>%s</b>%s' % (caption, ('\n%s' % rest_text) if rest_text else '')
            if message_data.get('link'):

                t = t.replace(message_data.get('link'), '')
                t.strip()
                t = '%s\n<a href="%s">Читать</a>' % (t, message_data.get('link'))

            elif message_data.get('href') \
                    and not re.findall(URL_PATTERN, message_data.get('text')) \
                    and 'vk.cc' not in message_data.get('text'):
                t = '%s\n<a href="%s">Читать%s</a>' % (
                    t,
                    message_data.get('href'),
                    (' на %s' % href_text) if href_text else '' )
        else:

            if message_data.get('link'):
                t = '%s\n%s' % (message_data.get('text').replace(message_data.get('link'), ''), message_data.get('link'))
            elif not re.findall(URL_PATTERN, message_data.get('text')):
                t = '%s\n%s' % (message_data.get('text'), message_data.get('href'))
            else:
                t = message_data.get('text')
        return t

    bot = telebot.TeleBot(bot_token)

    text = format_text(only_text=True) if message_data.get('text') else ''

    text_small = format_text(only_text=False) if message_data.get('text') else ''

    # print text_small

    if message_data.get('text'):

        if message_data.get('photo') and len(message_data.get('photo')) == 1 and len(text_small) <= 200:
            r = requests.get(message_data.get('photo')[0])
            file_path = os.path.join(settings.TEMP_DIR, message_data.get('photo')[0].split('/')[-1])
            with open(file_path, 'wb') as fd:
                for chunk in r:
                    fd.write(chunk)
            with open(file_path, 'rb') as fd:
                bot.send_photo(channel, fd, caption=text_small)

        else:
            bot.send_message(channel, text=text,
                             parse_mode='HTML', disable_web_page_preview=True)

    # if message_data.get('video') and len(message_data.get('video')) == 1:
    #     time.sleep(0.5)
    #     bot.send_message(channel, message_data.get('video')[0])

    if message_data.get('photo') and len(message_data.get('photo')) == 1 and len(text_small) > 200:
        time.sleep(0.5)
        r = requests.get(message_data.get('photo')[0])
        file_path = os.path.join(settings.TEMP_DIR, message_data.get('photo')[0].split('/')[-1])
        with open(file_path, 'wb') as fd:
            for chunk in r:
                fd.write(chunk)
        with open(file_path, 'rb') as fd:
            bot.send_photo(channel, fd)

    if message_data.get('photo') and len(message_data.get('photo')) > 1:
        msg = '<a href="%s">%s фото%s</a>' % (
            message_data.get('href'),
            str(len(message_data.get('photo'))),
            (' на %s' % href_text) if href_text else ''
        )
        bot.send_message(channel, text=msg, parse_mode='HTML', disable_web_page_preview=True)
        # if len(message_data.get('photo')) == 1:
        #     time.sleep(0.5)
        #     r = requests.get(message_data.get('photo')[0])
        #     file_path = os.path.join(settings.TEMP_DIR, message_data.get('photo')[0].split('/')[-1])
        #     with open(file_path, 'wb') as fd:
        #         for chunk in r:
        #             fd.write(chunk)
        #     with open(file_path, 'rb') as fd:
        #         bot.send_photo(channel, fd)
        # if len(message_data.get('photo')) > 1:

